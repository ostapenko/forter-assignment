import mongoose, { Connection } from 'mongoose';

export enum UserRole {
	'user' = 'user',
	'bot' = 'bot',
}
export interface IUser {
	id: string;
	nickname: string;
	lastSeen: Date;
	role: UserRole;
}

export interface IUserModel extends Omit<IUser, 'id'>, mongoose.Document {}

const UserSchema = new mongoose.Schema(
	{
		nickname: { type: mongoose.Schema.Types.String, index: true, required: true },
		lastSeen: { type: mongoose.Schema.Types.Date, required: true },
		role: {
			type: mongoose.Schema.Types.String,
			required: true,
			enum: [UserRole.user, UserRole.bot],
		},
	},
	{ collection: 'users', timestamps: true },
);

const defineUsers = (connection: Connection) => connection.model<IUserModel>('Users', UserSchema);

export { defineUsers };
