/* eslint-disable @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-unsafe-member-access */
// noinspection TypeScriptCheckImport

import './init';

import cors from '@koa/cors';
import KoaRouter from '@koa/router';
import Debug from 'debug';
import * as http from 'http';
import { StatusCodes } from 'http-status-codes';
import Koa from 'koa';
import koaBodyParser from 'koa-bodyparser';
import koaRequestIdMiddleware from 'koa-requestid';
import koaResponseTimeMiddleware from 'koa-response-time';

import { UserRole } from '../src/app/shared/types';
import config from './config';
import createBaseResponse from './helpers/base-response';
import EntitiesCacher, { CacheNamespace } from './helpers/entities-cacher';
import ResponseError, { ResponseErrorCode } from './helpers/response-error';
import { ContentType } from './models/ChatEntries';
import { IUser } from './models/Users';
import BotEngine from './modules/bot-engine';
import DatabaseModule from './modules/database';
import * as mongodb from './mongodb';

const debug = Debug('main');

process.on('uncaughtException', (error: Error) => {
	debug('uncaughtException', error);
	console.error(error);
});

process.on('unhandledRejection', (reason: any | null | undefined, promise: Promise<any>) => {
	debug('unhandledRejection', reason, promise);
	console.error(reason);
	console.dir(promise);
});

/**
 *
 */
async function run() {
	const { LISTEN_PORT } = config;
	const application = new Koa();
	const router = new KoaRouter();

	const mongooseConnection = await mongodb.connect();
	const databaseModule = new DatabaseModule(mongooseConnection);
	const botEngine = new BotEngine(databaseModule, { pollingIntervalMs: 50 });

	const entitiesCacher = new EntitiesCacher();
	entitiesCacher.addNamespace(CacheNamespace.UserStatus, {
		checkPeriodInSeconds: 10,
		maxCacheSize: 50,
		ttlInSeconds: 60,
	});

	application.use(koaResponseTimeMiddleware({ hrtime: true }));
	application.use(
		koaRequestIdMiddleware({
			expose: 'X-Forter-Request-Id',
			header: 'X-Request-Id',
		}),
	);
	application.use(koaBodyParser());
	application.use(cors());
	application.use(async (ctx, next) => {
		try {
			await next();
		} catch (e) {
			if (e instanceof ResponseError) {
				ctx.body = createBaseResponse(
					{
						error: e.errorCode,
						message: e.message,
					},
					e.statusCode,
				);
				return;
			}

			// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
			const emessage: string = (e as any)?.message ?? '<empty>';
			ctx.status = 500;
			ctx.body = createBaseResponse(
				{
					error: ResponseErrorCode.internal,
					message: emessage,
				},
				StatusCodes.INTERNAL_SERVER_ERROR,
			);
		}
	});

	router.get('/api/ping', ctx => {
		ctx.body = createBaseResponse('ok');
	});

	const publicApi = new KoaRouter({ prefix: '/api/v2' });
	publicApi.post('/entries', async ctx => {
		const { me, filters } = ctx.request.body as Partial<{
			me: string;
			filters: Partial<{ noLatterThan: Date | null }>;
		}>;

		if (!me) {
			throw new ResponseError(
				StatusCodes.BAD_REQUEST,
				ResponseErrorCode.invalid,
				"Bad request: empty 'me'",
			);
		}

		if (!filters) {
			throw new ResponseError(
				StatusCodes.BAD_REQUEST,
				ResponseErrorCode.invalid,
				"Bad request: empty 'filters'",
			);
		}

		await databaseModule.touchUser(me);

		const entries = await databaseModule.queryEntries(filters.noLatterThan);

		const requestedUsersByNicknames = new Set<string>();
		for (const entry of entries) {
			requestedUsersByNicknames.add(entry.author);
		}

		const usersToFetchByNicknames = new Set<string>([me]);
		const usersRecord: Record<string, IUser> = {};
		requestedUsersByNicknames.forEach(userNickname => {
			const maybeUser = entitiesCacher.getItem<IUser>(
				CacheNamespace.UserStatus,
				userNickname,
			);
			if (maybeUser) {
				usersRecord[maybeUser.nickname] = maybeUser;
			} else {
				usersToFetchByNicknames.add(userNickname);
			}
		});

		const users = await databaseModule.queryUsers(Array.from(usersToFetchByNicknames.values()));
		users.forEach(fetchedUser => {
			usersRecord[fetchedUser.nickname] = fetchedUser;
			entitiesCacher.cacheItem(CacheNamespace.UserStatus, fetchedUser.nickname, fetchedUser);
		});

		ctx.body = createBaseResponse({
			users: usersRecord,
			entries,
			whoami: me,
		});
	});

	publicApi.post('/message', async ctx => {
		const { me, content } = ctx.request.body as Partial<{
			me: string;
			content: string;
		}>;

		if (!me) {
			throw new ResponseError(
				StatusCodes.BAD_REQUEST,
				ResponseErrorCode.invalid,
				"Bad request: empty 'me'",
			);
		}

		if (!content) {
			throw new ResponseError(
				StatusCodes.BAD_REQUEST,
				ResponseErrorCode.invalid,
				"Bad request: empty 'content'",
			);
		}

		await databaseModule.submitMessage(content, ContentType.plain, {
			nickname: me,
			role: UserRole.user,
		});
		botEngine.scheduleTask(content, me);

		ctx.body = createBaseResponse({
			whoami: me,
		});
	});

	// router middlewares' order matters
	application.use(publicApi.routes());
	application.use(router.routes());

	const server = http.createServer(application.callback());

	server.listen(LISTEN_PORT, () => {
		debug(`http server has started, listening port ${LISTEN_PORT}`);
	});
	debug('application has started');
}

run().catch(e => {
	debug(e);
});
