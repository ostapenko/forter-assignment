module.exports = {
	apps: [
		{
			script: './backend/start.js',
			watch: false,
			source_map_support: false,
			exec_mode: 'cluster',
			instance_var: 'NODE_APP_INSTANCE',
			name: 'forter-assignment-nodejs',
			log_file: '/root/.pm2/logs/forter-assignment-nodejs-all-logs.log',
			merge_logs: true,
			interpreter_args: '--harmony --max_old_space_size=2048',
			env: {
				NODE_ENV: 'production',
			},
			env_production: {
				NODE_ENV: 'production',
			},
			max_memory_restart: '2G',
			instances: 1,
		},
	],
};
