import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AngularSvgIconModule } from 'angular-svg-icon';

import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { AvatarComponent } from './avatar/avatar.component';
import { ButtonComponent } from './button/button.component';
import { IconComponent } from './icon/icon.component';
import { MessageBubbleComponent } from './message-bubble/message-bubble.component';
import { PanelComponent } from './panel/panel.component';
import { RichTextComponent } from './rich-text/rich-text.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { TextComponent } from './text/text.component';
import { TextInputComponent } from './text-input/text-input.component';
import { ToggleComponent } from './toggle/toggle.component';

@NgModule({
	exports: [
		IconComponent,
		AvatarComponent,
		SpinnerComponent,
		ToggleComponent,
		TextComponent,
		ButtonComponent,
		MessageBubbleComponent,
		PanelComponent,
		TextInputComponent,
		RichTextComponent,
	],
	declarations: [
		IconComponent,
		AvatarComponent,
		SpinnerComponent,
		ToggleComponent,
		TextComponent,
		ButtonComponent,
		MessageBubbleComponent,
		PanelComponent,
		TextInputComponent,
		RichTextComponent,
	],
	imports: [
		RouterModule,
		CommonModule,
		AngularSvgIconModule,
		ReactiveFormsModule,
		FormsModule,
		SharedModule,
		CoreModule,
	],
})
export class UiKitModule {}
