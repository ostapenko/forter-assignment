import { Component, ViewChild } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { expect } from '@jest/globals';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { EnhancedUser, UserRole } from '../../shared/types';
import { UiKitModule } from '../../ui-kit/ui-kit.module';
import { ChatroomHeaderComponent } from './chatroom-header.component';

@Component({
	selector: `test-host-component`,
	template: ` <forter-chatroom-header></forter-chatroom-header>`,
})
class TestHostComponent {
	@ViewChild(ChatroomHeaderComponent)
	public header!: ChatroomHeaderComponent;
}

describe('ChatroomHeaderComponent', () => {
	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [AngularSvgIconModule, UiKitModule],
			declarations: [ChatroomHeaderComponent, TestHostComponent],
		}).compileComponents();
	});

	const mockUser = (isActive: boolean, role = UserRole.user): EnhancedUser =>
		({
			id: '',
			lastSeen: new Date(),
			isActive,
			role,
		} as EnhancedUser);
	const cases: Array<{ expectedHeadline: string; users: EnhancedUser[] }> = [
		{
			expectedHeadline: 'No online members',
			users: [],
		},
		{
			expectedHeadline: '1 online member',
			users: [mockUser(true)],
		},
		{
			expectedHeadline: '2 online members',
			users: [mockUser(true), mockUser(true)],
		},
		{
			expectedHeadline: '3 online members',
			users: [mockUser(true), mockUser(true), mockUser(true)],
		},
		{
			expectedHeadline: '4 online members',
			users: [mockUser(true), mockUser(true), mockUser(true), mockUser(true)],
		},
		{
			expectedHeadline: 'and 1 more online member',
			users: [mockUser(true), mockUser(true), mockUser(true), mockUser(true), mockUser(true)],
		},
		{
			expectedHeadline: 'and 5 more online members',
			users: [
				mockUser(true),
				mockUser(true),
				mockUser(true),
				mockUser(true),
				mockUser(true),
				mockUser(true),
				mockUser(true),
				mockUser(true),
				mockUser(true),
			],
		},
		{
			expectedHeadline: '1 online member',
			users: [mockUser(true), mockUser(false), mockUser(false), mockUser(true, UserRole.bot)],
		},
	];

	for (const { expectedHeadline, users } of cases) {
		it(`should display ${JSON.stringify(expectedHeadline)}`, () => {
			const fixture = TestBed.createComponent(TestHostComponent);
			fixture.detectChanges();
			fixture.componentInstance.header.users = users;
			fixture.detectChanges();

			const headlineEl = fixture.debugElement.query(By.css('.test-id-headline'));

			// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
			expect(headlineEl.nativeElement.textContent).toBe(expectedHeadline);
		});
	}
});
