import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import type { Meta, Story } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { addSeconds } from 'date-fns';
import { ContentType } from 'src/app/shared/types';

import { SafeRichTextPipe } from '../../core/pipes/safe-rich-text.pipe';
import { AvatarComponent } from '../avatar/avatar.component';
import { IconComponent } from '../icon/icon.component';
import { RichTextComponent } from '../rich-text/rich-text.component';
import { TextComponent } from '../text/text.component';
import { MessageBubbleComponent } from './message-bubble.component';

export default {
	title: 'MessageBubble',
	component: MessageBubbleComponent,
	decorators: [
		moduleMetadata({
			declarations: [
				MessageBubbleComponent,
				IconComponent,
				TextComponent,
				AvatarComponent,
				RichTextComponent,
				SafeRichTextPipe,
			],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				AngularSvgIconModule.forRoot(),
			],
		}),
	],
} as Meta;

export const MessageBubbleBase: Story<MessageBubbleComponent> = () => ({
	props: {
		entries: [
			{
				author: 'Terminator',
				id: '1',
				role: 'admin',
				date: new Date(),
				content: 'Hi there',
				contentType: ContentType.richText,
			},
			{
				author: 'Terminator',
				id: '2',
				date: addSeconds(new Date(), 12),
				content: [
					'<code>/joke</code>',
					'Attached to this <b>email</b> is a <em>full description of the challenge</em>.',
				],
				contentType: ContentType.richText,
			},
			{
				author: 'Terminator',
				id: '3',
				date: addSeconds(new Date(), 20),
				content:
					'You’ll have a link to a skeleton of the code you’ll need to get started. Be creative (design & UX) and playful (chatbot with an attitude?), yet also serious and thoughtful. Add relevant tests, write robust code that you’ll be proud of, etc.',
				contentType: ContentType.richText,
			},
			{
				author: 'Terminator',
				id: '4',
				date: addSeconds(new Date(), 22),
				content: "If you have any questions please contact Yosef or Ori who are CC'd.",
				contentType: ContentType.richText,
			},
		],
		entries2: [
			{
				author: 'SaraConnor',
				id: '5',
				role: 'moderator',
				date: new Date(),
				content: 'Hi there',
			},
			{
				author: 'SaraConnor',
				id: '6',
				date: addSeconds(new Date(), 12),
				content: 'Attached to this email is a full description of the challenge.',
			},
			{
				author: 'SaraConnor',
				id: '7',
				date: addSeconds(new Date(), 20),
				content:
					'You’ll have a link to a skeleton of the code you’ll need to get started. Be creative (design & UX) and playful (chatbot with an attitude?), yet also serious and thoughtful. Add relevant tests, write robust code that you’ll be proud of, etc.',
			},
			{
				author: 'SaraConnor',
				id: '8',
				date: addSeconds(new Date(), 22),
				content: "If you have any questions please contact Yosef or Ori who are CC'd.",
			},
		],

		entriesTrackBy: (index: number, entry: { id: string }): string => {
			return entry.id;
		},
	},
	template: `
		<div style="padding: 0; max-width: 480px; border: whitesmoke 2px solid; margin-left: 36px">
			<ui-message-bubble *ngFor="let entry of entries; first as isFirst; trackBy: entriesTrackBy"
				[authorRole]="entry.role"
				[author]="entry.author"
				[timestamp]="entry.date"
				[content]="entry.content"
				[contentType]="entry.contentType"
				[showHeadline]="isFirst"
			/>
			<ui-message-bubble *ngFor="let entry of entries2; first as isFirst; trackBy: entriesTrackBy"
				[authorRole]="entry.role"
				[author]="entry.author"
				[timestamp]="entry.date"
				[content]="entry.content"
				[showHeadline]="isFirst"
			/>
		</div>
	`,
});

MessageBubbleBase.story = {
	name: 'examples',
};
