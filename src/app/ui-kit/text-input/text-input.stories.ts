import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { forwardRef } from '@angular/core';
import { FormControl, FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { Meta, moduleMetadata, Story } from '@storybook/angular';

import { TextComponent } from '../text/text.component';
import { TextInputComponent } from './text-input.component';

export default {
	title: 'TextInput',
	component: TextInputComponent,
	decorators: [
		moduleMetadata({
			declarations: [TextInputComponent, TextComponent],
			imports: [BrowserModule, CommonModule, FormsModule, ReactiveFormsModule],
			providers: [
				{ provide: APP_BASE_HREF, useValue: '/' },
				{
					provide: NG_VALUE_ACCESSOR,
					// eslint-disable-next-line @typescript-eslint/no-use-before-define
					useExisting: forwardRef(() => TextInputComponent),
					multi: true,
				},
			],
		}),
	],
} as Meta;

export const TextInputBase: Story<TextInputComponent> = () => ({
	props: {
		firstInput: '',
		secondInput: 'Hello World',
		thirdInput: new FormControl('FormControl input'),
	},
	template: `
		<div style="padding: 24px; max-width: 480px;">
			<ui-text-input placeholder="Start typing..." style="margin-bottom: 4px" [(ngModel)]="firstInput"></ui-text-input> <br>
			<ui-text-input style="margin-bottom: 4px" [(ngModel)]="secondInput"></ui-text-input> <br>
			<ui-text-input style="margin-bottom: 4px" [formControl]="thirdInput"></ui-text-input> <br> <br>
			<code>| first = {{firstInput}} |</code>
			<code>| second = {{secondInput}} |</code>
			<code>| third = {{thirdInput.value}} |</code>
		</div>
	`,
});

TextInputBase.story = {
	name: 'example',
};
