import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface MessageInputFormValues {
	messageInput: string;
}

@Component({
	selector: 'forter-chatroom-footer',
	templateUrl: 'chatroom-footer.component.html',
	styleUrls: ['chatroom-footer.component.css'],
})
export class ChatroomFooterComponent implements OnInit, OnDestroy {
	@Input() sendIcon: string = '';
	@Output() newMessageSent = new EventEmitter<string>();
	messageInputForm: FormGroup;
	messageInput = '';
	private destroy: ReplaySubject<null> = new ReplaySubject(1);

	constructor(private formBuilder: FormBuilder) {
		const { messageInput } = this;
		this.messageInputForm = this.formBuilder.group({
			messageInput: [messageInput],
		});
	}

	isEmpty(): boolean {
		return this.messageInput.trim() === '';
	}

	ngOnInit(): void {
		this.messageInputForm.valueChanges
			.pipe(takeUntil(this.destroy))
			.subscribe((formValues: MessageInputFormValues) => {
				this.messageInput = formValues.messageInput;
			});
	}

	onSubmit(): void {
		if (this.messageInput) {
			console.info('Your message has been submitted', this.messageInput);
			this.newMessageSent.emit(this.messageInput);
		}
		this.messageInputForm.reset();
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();
	}
}
