import { StatusCodes } from 'http-status-codes';
import { JsonValue } from 'type-fest';

import { DateHelper } from './date';

export default function createBaseResponse(
	body: JsonValue | any,
	status: number = StatusCodes.OK,
): JsonValue | any {
	return {
		serverTime: DateHelper.zonedTimeToUTC().toISOString(),
		status,
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		body,
	};
}
