import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
	selector: 'ui-panel',
	templateUrl: 'panel.component.html',
	styleUrls: ['panel.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PanelComponent implements OnInit {
	constructor() {}

	ngOnInit(): void {}

	classes(): string {
		return ``;
	}
}
