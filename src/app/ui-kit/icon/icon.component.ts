import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

type IconColor = 'base' | 'dark' | 'light' | 'inherit';
type IconSize = 'base' | 'giant';

@Component({
	selector: 'ui-icon',
	templateUrl: 'icon.component.html',
	styleUrls: ['icon.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent implements OnInit {
	@Input() source = '';
	@Input() color: IconColor = 'base';
	@Input() size: IconSize = 'base';

	constructor() {}

	ngOnInit(): void {}

	classes(): string {
		return `color-${this.color} size-${this.size}`;
	}

	rootClasses(): Record<string, boolean> {
		return {
			root: true,
			[`size-${this.size}`]: true,
		};
	}

	svgDataUri(): string {
		if (this.source.endsWith('.svg')) {
			return this.source;
		}
		return `data:image/svg+xml;charset=utf-8,${this.source}`;
	}
}
