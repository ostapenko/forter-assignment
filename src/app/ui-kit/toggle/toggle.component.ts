import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	forwardRef,
	OnInit,
	ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
	selector: 'ui-toggle',
	templateUrl: 'toggle.component.html',
	styleUrls: ['toggle.component.css'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			// eslint-disable-next-line @typescript-eslint/no-use-before-define
			useExisting: forwardRef(() => ToggleComponent),
			multi: true,
		},
	],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToggleComponent implements OnInit, ControlValueAccessor {
	@ViewChild('checkbox') checkboxRef?: ElementRef<HTMLInputElement>;
	private onChange?: (value: any) => void;
	private onTouched?: () => void;

	constructor(private changeDetectorRef: ChangeDetectorRef) {}

	_checked = false;

	get checked(): boolean {
		return this.checkboxRef?.nativeElement.checked === true;
	}

	set checked(value: boolean) {
		this._checked = value;
		this.changeDetectorRef.detectChanges();
	}

	ngOnInit(): void {}

	onChangeInput(): void {
		this.writeValue(this.checked);
	}

	registerOnChange(fn?: () => void): void {
		this.onChange = fn;
	}

	registerOnTouched(fn?: () => void): void {
		this.onTouched = fn;
	}

	writeValue(value: boolean): void {
		this.checked = value;
		if (typeof this.onTouched === 'function') {
			this.onTouched();
		}
		if (typeof this.onChange === 'function') {
			this.onChange(value);
		}
	}

	toggleValue(event: Event): void {
		event.preventDefault();
		const newChecked = !this._checked;
		if (this.checkboxRef) {
			this.checkboxRef.nativeElement.checked = newChecked;
		}
		this.writeValue(newChecked);
	}
}
