import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AngularSvgIconModule } from 'angular-svg-icon';

import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { UiKitModule } from './ui-kit/ui-kit.module';

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		SharedModule,
		UiKitModule,
		ComponentsModule,
		RouterModule,
		CommonModule,
		ReactiveFormsModule,
		FormsModule,
		HttpClientModule,
		CoreModule,
		AngularSvgIconModule.forRoot(),
		RouterModule.forRoot([]),
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
