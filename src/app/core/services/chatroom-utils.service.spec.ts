import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { expect } from '@jest/globals';
import { addSeconds, subHours, subMinutes } from 'date-fns';

import {
	BackendEntry,
	BackendUser,
	ContentType,
	EnhancedUser,
	EnrichedEntry,
	UserRole,
	UsersMap,
} from '../../shared/types';
import { mockAvatarUrl } from '../../shared/utils/avatar.utils';
import { ChatroomService } from './chatroom.service';
import { ChatroomUtilsService } from './chatroom-utils.service';

describe('ChatroomUtilsService', () => {
	let service: ChatroomUtilsService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientModule],
			providers: [ChatroomService, ChatroomUtilsService],
		});
		service = TestBed.inject(ChatroomUtilsService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('parseBackendUser', () => {
		const date = new Date();
		const id = 'id';
		const nickname = 'nickname';
		const role = UserRole.user;
		const backendResponse = JSON.parse(
			JSON.stringify({
				lastSeen: date,
				id,
				role,
				nickname,
			}),
		) as BackendUser;

		expect(service.parseBackendUser(backendResponse)).toEqual({
			avatarUrl: mockAvatarUrl(nickname),
			id: id,
			isActive: false,
			lastSeen: date,
			nickname: nickname,
			role: role,
		} as EnhancedUser);
	});

	it('parseBackendEntry', () => {
		const date = new Date();
		const id = 'id';
		const author = 'nickname';
		const content = 'multiline\ncontent';
		const contentType = ContentType.plain;
		const backendResponse = JSON.parse(
			JSON.stringify({
				sent: date,
				id,
				author,
				content,
				contentType,
			}),
		) as BackendEntry;

		expect(
			service.parseBackendEntry(
				backendResponse,
				new Map([[author, { role: UserRole.user } as EnhancedUser]]),
				author,
			),
		).toEqual({
			id: id,
			author: author,
			authorRole: UserRole.user,
			contentType: contentType,
			content: ['multiline', 'content'],
			timestampPrev: new Date(0),
			timestamp: date,
			firstEntryOfThatAuthor: false,
			isMe: true,
			isBot: false,
		} as EnrichedEntry);

		expect(
			service.parseBackendEntry(
				backendResponse,
				new Map([[author, { role: UserRole.bot } as EnhancedUser]]),
				'anon',
			),
		).toEqual({
			id: id,
			author: author,
			authorRole: UserRole.bot,
			content: ['multiline', 'content'],
			contentType: contentType,
			timestampPrev: new Date(0),
			timestamp: date,
			firstEntryOfThatAuthor: false,
			isMe: false,
			isBot: true,
		} as EnrichedEntry);
	});

	it('joinEntriesArrays', () => {
		const mockEntry = (id: number): EnrichedEntry => ({ id: String(id) } as EnrichedEntry);

		expect(service.joinEntriesArrays([], [])).toEqual([]);
		expect(service.joinEntriesArrays([mockEntry(1), mockEntry(2)], [])).toEqual([
			mockEntry(1),
			mockEntry(2),
		]);
		expect(service.joinEntriesArrays([], [mockEntry(3), mockEntry(4)])).toEqual([
			mockEntry(3),
			mockEntry(4),
		]);
		expect(
			service.joinEntriesArrays([mockEntry(1), mockEntry(2)], [mockEntry(3), mockEntry(4)]),
		).toEqual([mockEntry(1), mockEntry(2), mockEntry(3), mockEntry(4)]);

		expect(
			service.joinEntriesArrays([mockEntry(1), mockEntry(2)], [mockEntry(1), mockEntry(2)]),
		).toEqual([mockEntry(1), mockEntry(2)]);
		expect(service.joinEntriesArrays([mockEntry(1)], [mockEntry(1), mockEntry(2)])).toEqual([
			mockEntry(1),
			mockEntry(2),
		]);

		expect(
			service.joinEntriesArrays(
				[mockEntry(1), mockEntry(2), mockEntry(3)],
				[mockEntry(2), mockEntry(3), mockEntry(4)],
			),
		).toEqual([mockEntry(1), mockEntry(2), mockEntry(3), mockEntry(4)]);
	});

	it('recalcActiveUsers', () => {
		const mockUser = (id: number, lastSeen: Date): EnhancedUser =>
			({ id: String(id), nickname: String(id), lastSeen } as EnhancedUser);
		const now = new Date();
		const minuteAgo = subMinutes(now, 1);
		const hourAgo = subHours(now, 1);
		const users: UsersMap = new Map(
			[mockUser(1, now), mockUser(2, minuteAgo), mockUser(3, hourAgo)].map(user => [
				user.nickname,
				user,
			]),
		);

		const changedMap = service.recalcActiveUsers(users, 5 * 60);
		expect(changedMap.size).toBe(3);
		expect(changedMap.get('1')).toMatchObject({ isActive: true });
		expect(changedMap.get('2')).toMatchObject({ isActive: true });
		expect(changedMap.get('3')).toMatchObject({ isActive: false });
	});

	it('addContextToEntries', () => {
		const mockEntry = (id: number, timestamp: Date, author: string): EnrichedEntry =>
			({ id: String(id), timestamp, author } as EnrichedEntry);

		const startDate = new Date();
		const array: EnrichedEntry[] = [
			mockEntry(0, addSeconds(startDate, 0), 'user1'),
			mockEntry(1, addSeconds(startDate, 1), 'user1'),
			mockEntry(2, addSeconds(startDate, 2), 'user1'),
			mockEntry(3, addSeconds(startDate, 3), 'user2'),
			mockEntry(4, addSeconds(startDate, 4), 'user2'),
			mockEntry(5, addSeconds(startDate, 5), 'user1'),
			mockEntry(6, addSeconds(startDate, 6), 'user2'),
		];

		const actual = service.addContextToEntries(array);
		expect(actual).toMatchObject([
			{ id: '0', timestampPrev: new Date(0), firstEntryOfThatAuthor: true },
			{ id: '1', timestampPrev: addSeconds(startDate, 0), firstEntryOfThatAuthor: false },
			{ id: '2', timestampPrev: addSeconds(startDate, 1), firstEntryOfThatAuthor: false },
			{ id: '3', timestampPrev: addSeconds(startDate, 2), firstEntryOfThatAuthor: true },
			{ id: '4', timestampPrev: addSeconds(startDate, 3), firstEntryOfThatAuthor: false },
			{ id: '5', timestampPrev: addSeconds(startDate, 4), firstEntryOfThatAuthor: true },
			{ id: '6', timestampPrev: addSeconds(startDate, 5), firstEntryOfThatAuthor: true },
		]);
	});
});
