import { Component, ViewChild } from '@angular/core';
import { TestBed } from '@angular/core/testing';

import { expect } from '@jest/globals';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { UserRole } from '../../shared/types';
import { UiKitModule } from '../ui-kit.module';
import { MessageBubbleComponent } from './message-bubble.component';

@Component({
	selector: `test-host-component`,
	template: `
		<ui-message-bubble
			content="testcontent"
			author="testauthor"
			[showHeadline]="showHeadline"
			[authorRole]="authorRole"
			[timestamp]="timestamp"
		></ui-message-bubble>
	`,
})
class TestHostComponent {
	@ViewChild(MessageBubbleComponent)
	public bubble!: MessageBubbleComponent;

	showHeadline = false;
	authorRole = UserRole.user;
	timestamp = new Date(2000, 0, 1, 12, 30, 30);
}

describe('MessageBubbleComponent', () => {
	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [AngularSvgIconModule, UiKitModule],
			declarations: [MessageBubbleComponent, TestHostComponent],
		}).compileComponents();
	});

	it(`Display user's message`, () => {
		const fixture = TestBed.createComponent(TestHostComponent);
		fixture.detectChanges();

		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const element: HTMLElement = fixture.nativeElement;

		expect(element.textContent).not.toContain('@testauthor');
		expect(element.textContent).not.toContain('user');
		expect(element.textContent).toContain('testcontent');
		expect(element.textContent).not.toContain('Jan, 1');

		expect(element.innerHTML).toMatchSnapshot();
	});

	it(`Display bot's message`, () => {
		const fixture = TestBed.createComponent(TestHostComponent);
		fixture.detectChanges();
		fixture.componentInstance.showHeadline = true;
		fixture.componentInstance.authorRole = UserRole.bot;
		fixture.detectChanges();

		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const element: HTMLElement = fixture.nativeElement;

		expect(element.textContent).toContain('@testauthor');
		expect(element.textContent).toContain('bot');
		expect(element.textContent).toContain('testcontent');
		expect(element.textContent).toContain('Jan, 1');

		expect(element.innerHTML).toMatchSnapshot();
	});
});
