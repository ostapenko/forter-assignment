import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import type { Meta, Story } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { EmptyComponent } from './empty.component';

export default {
	title: 'Empty',
	component: EmptyComponent,
	decorators: [
		moduleMetadata({
			declarations: [EmptyComponent],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				AngularSvgIconModule.forRoot(),
			],
		}),
	],
} as Meta;

export const EmptyBase: Story<EmptyComponent> = () => ({
	props: {},
	template: `
		<div style="padding: 16px; max-width: 480px;">
			<ui-empty></ui-empty>
		</div>
	`,
});

EmptyBase.story = {
	name: 'examples',
};
