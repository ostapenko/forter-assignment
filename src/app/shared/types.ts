import { HttpStatusCode } from '@angular/common/http';

export enum ContentType {
	plain = 'plain',
	richText = 'richText',
}

export interface EnhancedUser {
	id: string;
	nickname: string;
	avatarUrl: string;
	role: UserRole;
	isActive: boolean;
	lastSeen: Date;
}

export interface EnrichedEntry {
	id: string;
	content: Array<string>;
	contentType: ContentType;
	author: string;
	authorRole: UserRole;
	timestampPrev: Date;
	timestamp: Date;
	firstEntryOfThatAuthor: boolean;

	isMe: boolean;
	isBot: boolean;
}

export type UsersMap = Map<string, EnhancedUser>;

export enum UserRole {
	user = 'user',
	bot = 'bot',
}

export interface BackendUser {
	id: string;
	nickname: string;
	lastSeen: string;
	role: UserRole;
}

export interface BackendEntry {
	id: string;
	author: string;
	sent: string;
	content: string;
	contentType: ContentType;
}

export interface BaseResponseShape<TBody> {
	status: HttpStatusCode;
	serverTime: string;
	body: TBody;
}

export interface SubmitMessageResponse {
	whoami: string;
}

export interface LoadEntriesResponse {
	whoami: string;
	users: Record<string, BackendUser>;
	entries: BackendEntry[];
}
