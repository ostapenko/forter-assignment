import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { Meta, moduleMetadata, Story } from '@storybook/angular';

import { SpinnerComponent } from './spinner.component';

export default {
	title: 'Spinner',
	component: SpinnerComponent,
	decorators: [
		moduleMetadata({
			declarations: [SpinnerComponent],
			imports: [BrowserModule, CommonModule],
		}),
	],
} as Meta;

export const SpinnerBase: Story<SpinnerComponent> = () => ({
	props: {},
	template: `
	<div style="padding: 32px; max-width: 480px">
		<ui-spinner></ui-spinner>
	</div>
	`,
});

SpinnerBase.story = {
	name: 'base view',
};
