import Debug from 'debug';
import mongoose from 'mongoose';

import config from './config';

const debug = Debug('mongodb');

async function connect(): Promise<mongoose.Connection> {
	const { MONGODB_CONNECTION_STRING } = config;
	debug('trying to connect to the database');

	const connection = mongoose.createConnection(MONGODB_CONNECTION_STRING, {
		autoCreate: true,
		autoIndex: true,
		tls: false,
		appName: 'forter-nodejs',
		w: 'majority',
	});

	try {
		// Use connect method to connect to the Server
		await connection;
		debug('connection has been established successfully');
	} catch (error) {
		debug('unable to connect to the database:', error);
		throw error;
	}

	return connection;
}

export { connect };
