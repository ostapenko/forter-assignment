import Debug from 'debug';
import rp from 'request-promise-native';
import { v4 as uuid } from 'uuid';

import { UserRole } from '../../src/app/shared/types';
import { ContentType } from '../models/ChatEntries';
import DatabaseModule from './database';

const debug = Debug('bot-engine');

interface BotEngineTask {
	id: string;
	requestQuery: string;
	requestAuthor: string;
}

export enum BotCommand {
	help = 'help',
	joke = 'joke',
}

interface BotEngineTaskParsed extends BotEngineTask {
	command: BotCommand;
	commandContext: string;
}

interface CommandProcessor {
	process: (task: BotEngineTaskParsed) => Promise<void>;
}

interface BotEngineParams {
	pollingIntervalMs: number;
}

const SLASH_COMMAND_REGEX = /^(\/\w+?)\b/g;

function extractSlashCommands(input: string): [string, string] | null {
	const maybeMatch = input.match(SLASH_COMMAND_REGEX);

	if (!maybeMatch) {
		return null;
	}

	const command = maybeMatch[0].toLowerCase();
	const context = input.slice(command.length).trim();

	return [command, context];
}

type Bot = { nickname: string; role: UserRole.bot };
const ChuckNorrisBot: Bot = {
	nickname: 'Chuck Norris Bot',
	role: UserRole.bot,
};

const ChuckNorrisCategories: string[] = [
	'dev',
	'science',
	'animal',
	'career',
	'celebrity',
	'explicit',
	'fashion',
	'food',
	'history',
	'money',
	'movie',
	'music',
	'political',
	'religion',
	'sport',
	'travel',
];
export default class BotEngine {
	private readonly pollingIntervalId: NodeJS.Timer | null = null;
	private tasksQueue: BotEngineTask[] = [];
	private readonly CommandProcessors: Record<BotCommand, CommandProcessor>;

	constructor(private readonly database: DatabaseModule, { pollingIntervalMs }: BotEngineParams) {
		this.pollingIntervalId = setInterval(this.pollTasks, pollingIntervalMs);

		this.CommandProcessors = {
			[BotCommand.help]: {
				process: this.commandHelp,
			},
			[BotCommand.joke]: {
				process: this.commandJoke,
			},
		};
	}

	commandHelp = async (task: BotEngineTaskParsed): Promise<void> => {
		await this.database.submitMessage(
			`Hello, <b>${task.requestAuthor}</b>!`,
			ContentType.richText,
			ChuckNorrisBot,
		);
		const helpMessage = `
My name's Chuck Norris. Please, use the following commands to interact in this chat:

* <code>/help</code> - for this message
* <code>/joke</code> - to get a random joke
* <code>/joke {category}</code> - to get a joke withing a certain category. All possible categories include: ${ChuckNorrisCategories.join(
			', ',
		)}
* <code>/joke {query}</code> - to get a joke about anything you like, use a word or a short phrase
		`.trim();
		await this.database.submitMessage(helpMessage, ContentType.richText, ChuckNorrisBot);
		return this.database.submitMessage('Enjoy!', ContentType.richText, ChuckNorrisBot);
	};

	commandJoke = async (task: BotEngineTaskParsed): Promise<void> => {
		const context = task.commandContext.toLowerCase().trim();
		const maybeCategoryWord = context
			.split(/\s/)
			.filter(str => !!str)
			.at(0);
		const maybeCategory: string | '' =
			ChuckNorrisCategories.find(item => maybeCategoryWord === item) ?? '';
		const maybeSearchQuery: string | '' = context;

		type RandomJokeResponse = { value: string };
		type SearchJokeResponse = { result: Array<{ value: string }> };

		console.time('api.chucknorris.io');
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const [randomJoke, searchJokes]: [RandomJokeResponse, SearchJokeResponse | null] =
			await Promise.all([
				rp('https://api.chucknorris.io/jokes/random', {
					json: true,
					qs: maybeCategory ? { category: maybeCategory } : {},
				}),
				maybeSearchQuery
					? rp('https://api.chucknorris.io/jokes/search', {
							json: true,
							qs: { query: maybeSearchQuery },
					  })
					: Promise.resolve(null),
			]);

		console.timeEnd('api.chucknorris.io');
		let jokeText = '';

		if (searchJokes?.result[0]) {
			jokeText = searchJokes?.result[0]?.value;
		}

		if (!jokeText) {
			jokeText = randomJoke.value;
		}

		await this.database.submitMessage(
			`Hello, <b>${task.requestAuthor}</b>!`,
			ContentType.richText,
			ChuckNorrisBot,
		);
		const message = `
Here is your joke:
<em>${jokeText}</em>
		`.trim();
		await this.database.submitMessage(message, ContentType.richText, ChuckNorrisBot);
		return this.database.submitMessage(
			'Would you like another joke?',
			ContentType.richText,
			ChuckNorrisBot,
		);
	};

	public scheduleTask(query: string, author: string): string {
		const id = uuid();
		this.tasksQueue.push({
			id,
			requestQuery: query,
			requestAuthor: author,
		});

		return id;
	}

	public destroy() {
		if (this.pollingIntervalId) {
			clearInterval(this.pollingIntervalId);
		}
	}

	private pollTasks = (): void => {
		const parsedTasks = this.tasksQueue
			.map(this.tryParseTask)
			.filter((it): it is BotEngineTaskParsed => it !== null);

		this.tasksQueue = [];

		for (const parsedTask of parsedTasks) {
			try {
				const { process } = this.CommandProcessors[parsedTask.command];
				process(parsedTask).catch(reason => this.onError(parsedTask, reason));
			} catch (e) {
				setImmediate(() => this.onError(parsedTask, e));
			}
		}
	};

	private tryParseTask(this: void, task: BotEngineTask): BotEngineTaskParsed | null {
		const { requestQuery, requestAuthor, id } = task;

		const maybeCommand = extractSlashCommands(requestQuery);

		if (maybeCommand) {
			const [commandString, commandContext] = maybeCommand;

			const command: BotCommand | null = (() => {
				switch (commandString) {
					case '/help':
						return BotCommand.help;
					case '/joke':
						return BotCommand.joke;
				}
				return null;
			})();
			if (command) {
				return {
					requestQuery,
					requestAuthor,
					id,
					command,
					commandContext,
				};
			}
		}

		return null;
	}

	private onError = (task: BotEngineTaskParsed, error: unknown): void => {
		debug(`Error happened while processing task #${task.id} ${task.command}`, error);
	};
}
