import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { Meta, moduleMetadata, Story } from '@storybook/angular';

import { CoreModule } from '../../core/core.module';
import { TextComponent } from '../text/text.component';
import { RichTextComponent } from './rich-text.component';

export default {
	title: 'RichText',
	component: RichTextComponent,
	decorators: [
		moduleMetadata({
			declarations: [RichTextComponent, TextComponent],
			imports: [BrowserModule, CommonModule, CoreModule],
		}),
	],
} as Meta;

export const RichTextBase: Story<RichTextComponent> = () => ({
	props: {
		html: `
		<code>/joke</code> <br/>
		Hello <strong>Alex</strong>! <br/>
		<br/>
		Here is your joke: <em>If Chuck Norris writes code with bugs, the bugs fix themselves.</em> <br/>

		<img src="404.png" onerror="console.log('XSS')" alt="xss!"/>
		`,
	},
	template: `
	<div style="padding: 16px; max-width: 480px">
		<ui-rich-text [content]="html"></ui-rich-text>
	</div>
	`,
});

RichTextBase.story = {
	name: 'example',
};
