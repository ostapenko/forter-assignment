import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import send from '@material-design-icons/svg/outlined/send.svg';
import type { Meta, Story } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { IconComponent } from './icon.component';

export default {
	title: 'Icon',
	component: IconComponent,
	decorators: [
		moduleMetadata({
			declarations: [IconComponent],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				AngularSvgIconModule.forRoot(),
			],
		}),
	],
} as Meta;

export const IconBase: Story<IconComponent> = () => ({
	props: {
		send,
	},
	template: `
		<h2>Full list of available icons can be found here: <a href="https://marella.me/material-design-icons/demo/svg/">material-design-icons/demo/svg</a></h2>
		<div style="padding: 16px; max-width: 480px;">
			<ui-icon [source]="send"></ui-icon>
			<ui-icon [source]="send"></ui-icon>
			<ui-icon [source]="send"></ui-icon> <br>
			<ui-icon [source]="send" size="giant"></ui-icon>
			<ui-icon [source]="send" size="giant"></ui-icon>
			<ui-icon [source]="send" size="giant"></ui-icon>
		</div>
		<div style="padding: 16px; max-width: 480px; background-color: #fff">
			<ui-icon color="dark" [source]="send"></ui-icon>
			<ui-icon color="dark" [source]="send"></ui-icon>
			<ui-icon color="dark" [source]="send"></ui-icon> <br>
			<ui-icon color="dark" [source]="send" size="giant"></ui-icon>
			<ui-icon color="dark" [source]="send" size="giant"></ui-icon>
			<ui-icon color="dark" [source]="send" size="giant"></ui-icon>
		</div>
		<div style="padding: 16px; max-width: 480px; background-color: #333">
			<ui-icon color="light" [source]="send"></ui-icon>
			<ui-icon color="light" [source]="send"></ui-icon>
			<ui-icon color="light" [source]="send"></ui-icon> <br>
			<ui-icon color="light" [source]="send" size="giant"></ui-icon>
			<ui-icon color="light" [source]="send" size="giant"></ui-icon>
			<ui-icon color="light" [source]="send" size="giant"></ui-icon>
		</div>
	`,
});

IconBase.story = {
	name: 'examples',
};
