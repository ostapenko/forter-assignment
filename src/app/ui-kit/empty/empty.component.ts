import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
	selector: 'ui-empty',
	templateUrl: 'empty.component.html',
	styleUrls: ['empty.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmptyComponent implements OnInit {
	constructor() {}

	ngOnInit(): void {}

	classes(): string {
		return ``;
	}
}
