import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import * as DOMPurify from 'isomorphic-dompurify';

@Pipe({
	name: 'safeRichText',
})
export class SafeRichTextPipe implements PipeTransform {
	constructor(protected sanitizer: DomSanitizer) {}

	public transform(value: string): SafeHtml {
		const sanitizedContent = DOMPurify.sanitize(value, {
			ALLOWED_TAGS: ['quote', 'q', 'code', 'b', 'strong', 'em', 'i', 'br', 'div', 'span'],
		});
		return this.sanitizer.bypassSecurityTrustHtml(sanitizedContent);
	}
}
