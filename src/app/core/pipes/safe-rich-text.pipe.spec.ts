import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { expect } from '@jest/globals';

import { SafeRichTextPipe } from './safe-rich-text.pipe';

describe('SafeRichTextPipe', () => {
	let pipe: SafeRichTextPipe;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientModule],
			providers: [SafeRichTextPipe],
		});
		pipe = TestBed.inject(SafeRichTextPipe);
	});

	it('should be created', () => {
		expect(pipe).toBeTruthy();
	});

	it('should keep safe tags', () => {
		const source = `
		<div><span>Text</span></div>
		<strong>Bold text</strong>
		<em>Italic text</em>
		<code>monospace text</code>
		`;

		expect(pipe.transform(source)).toMatchSnapshot();
	});

	it('should strip unsafe tags/attributes/scripts', () => {
		const source = `
		<script>alert('XSS');</script>
		<div style="color: red">red</div>
		<img src="404.png" onerror="console.log('XSS')" alt="xss!"/>
		`;

		expect(pipe.transform(source)).toMatchSnapshot();
	});
});
