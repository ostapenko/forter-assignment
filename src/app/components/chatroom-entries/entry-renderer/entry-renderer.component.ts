import { ChangeDetectionStrategy, Component, ElementRef, Input, OnInit } from '@angular/core';

import { differenceInSeconds } from 'date-fns';

import { ContentType, EnrichedEntry, UserRole } from '../../../shared/types';

@Component({
	selector: 'forter-entry-renderer',
	templateUrl: 'entry-renderer.component.html',
	styleUrls: ['entry-renderer.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntryRendererComponent implements OnInit {
	@Input() entry: EnrichedEntry = {
		id: '',
		author: '',
		authorRole: UserRole.user,
		content: [''],
		contentType: ContentType.plain,
		isBot: false,
		isMe: false,
		firstEntryOfThatAuthor: false,
		timestamp: new Date(0),
		timestampPrev: new Date(0),
	};

	constructor(public element: ElementRef<HTMLElement>) {}

	ngOnInit(): void {}

	roleToDisplay(): string {
		// eslint-disable-next-line sonarjs/no-small-switch
		switch (this.entry.authorRole) {
			case UserRole.bot:
				return 'bot';
		}
		return '';
	}

	shouldShowHeadline(): boolean {
		const { firstEntryOfThatAuthor, timestampPrev, timestamp } = this.entry;
		if (firstEntryOfThatAuthor) {
			return true;
		}

		return Math.abs(differenceInSeconds(timestamp, timestampPrev)) >= 120;
	}
}
