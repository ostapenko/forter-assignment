import { Component, Input, OnInit } from '@angular/core';

import { EnhancedUser, UserRole } from '../../shared/types';

@Component({
	selector: 'forter-chatroom-header',
	templateUrl: 'chatroom-header.component.html',
	styleUrls: ['chatroom-header.component.css'],
})
export class ChatroomHeaderComponent implements OnInit {
	@Input() users: EnhancedUser[] = [];

	constructor() {}

	ngOnInit(): void {}

	classes(): string {
		return ``;
	}

	activeUsers(): EnhancedUser[] {
		return this.users.filter(user => user.isActive && user.role === UserRole.user);
	}

	topFourActiveUsers(): EnhancedUser[] {
		const activeUsers = this.activeUsers();
		return activeUsers.sort((l, r) => r.lastSeen.valueOf() - l.lastSeen.valueOf()).slice(0, 4);
	}

	headline(): string {
		const activeUsersCount = this.activeUsers().length;
		if (activeUsersCount === 0) {
			return 'No online members';
		}
		if (activeUsersCount === 1) {
			return '1 online member';
		}
		if (activeUsersCount < 5) {
			return `${activeUsersCount.toString(10)} online members`;
		}
		if (activeUsersCount === 5) {
			return `and 1 more online member`;
		}
		return `and ${(activeUsersCount - 4).toString(10)} more online members`;
	}

	usersTrackBy(i: number, user: EnhancedUser): string {
		return user.id;
	}
}
