import Debug from 'debug';
import NodeCache from 'node-cache';

const debug = Debug('entities-cacher');

export enum CacheNamespace {
	UserStatus = 'UserStatus',
}

export interface CacheConfig {
	ttlInSeconds: number;
	maxCacheSize: number;
	checkPeriodInSeconds: number;
}

class CacheWrapper {
	private cache: NodeCache;

	constructor(
		public readonly namespace: CacheNamespace,
		private readonly ttl: number,
		private readonly maxKeys: number,
		private readonly checkPeriod: number,
	) {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-call
		this.cache = new NodeCache({
			forceString: false,
			maxKeys,
			stdTTL: ttl,
			checkperiod: checkPeriod,
			deleteOnExpire: true,
			useClones: true,
			errorOnMissing: false,
		});
	}

	set(key: string, value: any): void {
		try {
			this.cache.set(key, value);
		} catch {
			// ignore
		}
	}

	get(key: string): any | undefined {
		try {
			return this.cache.get(key);
		} catch {
			// ignore
		}
	}
}

export default class EntitiesCacher {
	private namespaces: Map<CacheNamespace, CacheWrapper> = new Map();

	constructor() {
		debug('constructor');
	}

	addNamespace(
		namespace: CacheNamespace,
		{ ttlInSeconds, maxCacheSize, checkPeriodInSeconds }: CacheConfig,
	) {
		const cacheWrapper = new CacheWrapper(
			namespace,
			ttlInSeconds,
			maxCacheSize,
			checkPeriodInSeconds,
		);

		this.namespaces.set(namespace, cacheWrapper);
	}

	cacheItem(namespace: CacheNamespace, key: string, value: any): void {
		const cacheWrapper = this.findCacheWrapper(namespace);

		cacheWrapper.set(key, value);
	}

	getItem<T>(namespace: CacheNamespace, key: string): T | undefined {
		const cacheWrapper = this.findCacheWrapper(namespace);

		return cacheWrapper.get(key) as T | undefined;
	}

	private findCacheWrapper(namespace: CacheNamespace): CacheWrapper {
		const cacheWrapper = this.namespaces.get(namespace);
		if (!cacheWrapper) {
			throw new Error(`No cacheWrapper for namespace ${namespace}`);
		}

		return cacheWrapper;
	}
}
