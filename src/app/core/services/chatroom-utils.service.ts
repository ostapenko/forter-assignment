import { Injectable } from '@angular/core';

import { differenceInSeconds, parseJSON, toDate } from 'date-fns';

import {
	BackendEntry,
	BackendUser,
	ContentType,
	EnhancedUser,
	EnrichedEntry,
	UserRole,
	UsersMap,
} from '../../shared/types';
import { mockAvatarUrl } from '../../shared/utils/avatar.utils';

@Injectable()
export class ChatroomUtilsService {
	constructor() {}

	parseBackendUser = (backendUser: BackendUser): EnhancedUser => {
		return {
			avatarUrl: mockAvatarUrl(backendUser.nickname),
			id: backendUser.id,
			isActive: false,
			lastSeen: parseJSON(backendUser.lastSeen),
			nickname: backendUser.nickname,
			role: backendUser.role,
		};
	};

	parseBackendEntry = (
		backendEntry: BackendEntry,
		usersMap: UsersMap,
		me: string,
	): EnrichedEntry => {
		const role = usersMap.get(backendEntry.author)?.role || UserRole.user;
		return {
			author: backendEntry.author,
			authorRole: role,
			content: backendEntry.content.split('\n'),
			contentType: backendEntry.contentType || ContentType.plain,
			firstEntryOfThatAuthor: false,
			id: backendEntry.id,
			isBot: role === UserRole.bot,
			isMe: backendEntry.author === me,
			timestamp: parseJSON(backendEntry.sent),
			timestampPrev: new Date(0),
		};
	};

	recalcActiveUsers = (
		usersMap: UsersMap,
		activeTimeoutSeconds: number,
	): Map<string, EnhancedUser> => {
		const now = new Date();
		usersMap.forEach(u => {
			u.isActive = differenceInSeconds(now, u.lastSeen) <= activeTimeoutSeconds;
		});
		return usersMap;
	};

	joinEntriesArrays = (
		earlyEntries: EnrichedEntry[],
		latterEntries: EnrichedEntry[],
	): EnrichedEntry[] => {
		if (earlyEntries.length === 0) {
			return latterEntries;
		}
		if (latterEntries.length === 0) {
			return earlyEntries;
		}

		const firstNewEntry = latterEntries[0];
		const firstNewEntryId = firstNewEntry.id;
		const overlapOffsetIndex = earlyEntries.findIndex(entry => entry.id === firstNewEntryId);
		if (overlapOffsetIndex === -1) {
			return [...earlyEntries, ...latterEntries];
		}

		return [...earlyEntries.slice(0, overlapOffsetIndex), ...latterEntries];
	};

	addContextToEntries = (entries: EnrichedEntry[]): EnrichedEntry[] => {
		const result: EnrichedEntry[] = [...entries];
		result[0].timestampPrev = new Date(0);
		result[0].firstEntryOfThatAuthor = true;

		for (let i = 1; i < result.length; i++) {
			const prev = result[i - 1];
			result[i].timestampPrev = toDate(prev.timestamp);
			result[i].firstEntryOfThatAuthor = result[i].author !== prev.author;
		}

		return result;
	};
}
