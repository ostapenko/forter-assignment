import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

type TextColor = 'primary' | 'secondary' | 'error' | 'dark' | 'light' | 'dimmed';

@Component({
	selector: 'ui-rich-text',
	templateUrl: 'rich-text.component.html',
	styleUrls: ['rich-text.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RichTextComponent {
	@Input() content: string = '';
	@Input() color: TextColor = 'primary';

	constructor() {}
}
