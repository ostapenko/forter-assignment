import { zonedTimeToUtc } from 'date-fns-tz';

export class DateHelper {
	public static zonedTimeToUTC(date?: Date): Date {
		return zonedTimeToUtc(date || new Date(), Intl.DateTimeFormat().resolvedOptions().timeZone);
	}
}
