import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { Meta, moduleMetadata, Story } from '@storybook/angular';

import { AvatarComponent } from './avatar.component';

export default {
	title: 'Avatar',
	component: AvatarComponent,
	decorators: [
		moduleMetadata({
			declarations: [AvatarComponent],
			imports: [BrowserModule, CommonModule],
		}),
	],
} as Meta;

const avatarUrls = [
	'https://robohash.org/alex@gmail.com?size=96x96&bgset=bg2',
	'https://robohash.org/jeremy@gmail.com?size=96x96&bgset=bg2',
	'https://robohash.org/susan@gmail.com?size=96x96&bgset=bg2',
];

export const AvatarBase: Story<AvatarComponent> = () => ({
	props: {
		avatarUrls,
	},
	template: `
	<div style="padding: 16px; max-width: 480px">
		<ui-avatar *ngFor="let avatarUrl of avatarUrls" [src]="avatarUrl"></ui-avatar>
		<br>
		<ui-avatar *ngFor="let avatarUrl of avatarUrls" [src]="avatarUrl" size="small"></ui-avatar>
		<br>
		<ui-avatar *ngFor="let avatarUrl of avatarUrls" [src]="avatarUrl" size="tiny" border="highlight" style="margin-left: -10px"></ui-avatar>
	</div>
	`,
});

AvatarBase.story = {
	name: 'examples',
};
