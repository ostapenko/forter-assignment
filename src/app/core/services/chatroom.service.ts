import { HttpClient, HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
	BehaviorSubject,
	catchError,
	interval,
	map,
	merge,
	Observable,
	ReplaySubject,
	retry,
	Subject,
	Subscription,
	switchMap,
	takeUntil,
	throttle,
	throwError,
	timer,
} from 'rxjs';

import {
	BaseResponseShape,
	EnrichedEntry,
	LoadEntriesResponse,
	SubmitMessageResponse,
	UsersMap,
} from '../../shared/types';
import { parseQuerySettings } from '../../shared/utils/parse-query-settings.utils';
import { ChatroomUtilsService } from './chatroom-utils.service';

const POLLING_INTERVAL = 5000;
const THROTTLE_INTERVAL = 1000;
const ACTIVE_USER_TIMEOUT_IN_SECONDS = 5 * 60;

export interface ChatroomState {
	nickname: string;
	users: UsersMap;
	entries: Array<EnrichedEntry>;
}

const emptyState: ChatroomState = {
	nickname: '',
	users: new Map(),
	entries: [],
};

@Injectable()
export class ChatroomService implements OnDestroy {
	private destroy: ReplaySubject<null> = new ReplaySubject(1);
	private chatroomEntriesPollingSubscription: Subscription | null = null;
	private chatroom: BehaviorSubject<ChatroomState> = new BehaviorSubject(emptyState);
	private forceUpdateSubject = new Subject<null>();

	public chatroomObservable$: Observable<ChatroomState> = this.chatroom.asObservable();

	private nickname: string = 'anon';
	private baseUrl: string =
		window.location.hostname === 'localhost'
			? 'http://localhost:3001'
			: 'https://freshauto.shopstory.live';

	constructor(
		private http: HttpClient,
		private route: ActivatedRoute,
		private chatroomUtilsService: ChatroomUtilsService,
	) {
		const { baseUrl, user } = parseQuerySettings();
		if (baseUrl) {
			this.baseUrl = baseUrl;
		}

		if (user) {
			this.nickname = user;
		}

		console.log('ChatroomService', {
			nickname: this.nickname,
			baseUrl: this.baseUrl,
		});

		this.chatroomObservable$ = this.chatroom.asObservable();
		this.subscribeOnChat();

		this.chatroom.next({
			...emptyState,
			nickname: this.nickname,
		});
	}

	private get submitMessageEndpoint(): string {
		return `${this.baseUrl}/api/v2/message`;
	}

	private get queryChatroomEntriesEndpoint(): string {
		return `${this.baseUrl}/api/v2/entries`;
	}

	private get defaultHeaders(): Record<string, string> {
		return {
			'Content-Type': 'application/json; charset=utf-8',
		};
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();

		this.chatroom.complete();
		this.chatroomEntriesPollingSubscription?.unsubscribe();
	}

	submitMessage(content: string): Observable<SubmitMessageResponse> {
		return this.http
			.post<BaseResponseShape<SubmitMessageResponse>>(
				this.submitMessageEndpoint,
				JSON.stringify({ me: this.nickname, content }),
				{
					headers: this.defaultHeaders,
					responseType: 'json',
				},
			)
			.pipe(
				retry(0),
				catchError(this.handleError),
				map(fullResponse => {
					if (fullResponse.status === HttpStatusCode.Ok) {
						return fullResponse.body;
					}
					throw fullResponse;
				}),
			);
	}

	updateChatroom(): void {
		this.forceUpdateSubject.next(null);
	}

	private subscribeOnChat(): void {
		this.chatroomEntriesPollingSubscription = merge(
			timer(0, POLLING_INTERVAL),
			this.forceUpdateSubject.asObservable(),
		)
			.pipe(
				throttle(() => interval(THROTTLE_INTERVAL)),
				switchMap(() => {
					return this.queryChatroomEntries();
				}),
				takeUntil(this.destroy),
			)
			.subscribe(this.newEntriesChunkReceived);
	}

	private queryChatroomEntries(): Observable<LoadEntriesResponse> {
		return this.http
			.post<BaseResponseShape<LoadEntriesResponse>>(
				this.queryChatroomEntriesEndpoint,
				JSON.stringify({ me: this.nickname, filters: {} }),
				{
					headers: this.defaultHeaders,
					responseType: 'json',
				},
			)
			.pipe(
				retry(1),
				catchError(this.handleError),
				map(fullResponse => {
					if (fullResponse.status === HttpStatusCode.Ok) {
						return fullResponse.body;
					}
					throw fullResponse;
				}),
			);
	}

	// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
	private handleError = <T>(error: HttpErrorResponse | BaseResponseShape<T>) => {
		if (error instanceof HttpErrorResponse) {
			if (error.error instanceof ErrorEvent) {
				// A client-side or network error occurred. Handle it accordingly.
				console.error('An error occurred:', error.error.message);
			} else {
				// The backend returned an unsuccessful response code.
				// The response body may contain clues as to what went wrong,
				console.error(
					`Backend returned code ${error.status}, ` + `body was: ${String(error.error)}`,
				);
			}
		}
		if ('status' in error && 'body' in error) {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${error.status}, ` + `body was: ${String(error.body)}`,
			);
		}
		// return an observable with a user-facing error message
		return throwError(() => new Error('Something bad happened; please try again later'));
	};

	private newEntriesChunkReceived = (entriesResponse: LoadEntriesResponse) => {
		const { users: knownUsers, entries: knownEntries } = this.chatroom.getValue();

		let allUsers = new Map(knownUsers.entries());

		for (const u of Object.values(entriesResponse.users)) {
			let enhancedUser = this.chatroomUtilsService.parseBackendUser(u);
			const maybeKnownUser = allUsers.get(enhancedUser.nickname);
			if (maybeKnownUser) {
				enhancedUser = Object.assign(maybeKnownUser, enhancedUser);
			}

			allUsers.set(enhancedUser.nickname, enhancedUser);
		}

		allUsers = this.chatroomUtilsService.recalcActiveUsers(
			allUsers,
			ACTIVE_USER_TIMEOUT_IN_SECONDS,
		);

		const newEntries = entriesResponse.entries.map(entry =>
			this.chatroomUtilsService.parseBackendEntry(entry, allUsers, this.nickname),
		);

		let allEntries = this.chatroomUtilsService.joinEntriesArrays(knownEntries, newEntries);
		allEntries = this.chatroomUtilsService.addContextToEntries(allEntries);

		this.chatroom.next({
			nickname: this.nickname,
			users: allUsers,
			entries: allEntries,
		});
	};
}
