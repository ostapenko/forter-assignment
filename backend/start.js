/* eslint-disable @typescript-eslint/no-var-requires */

require('./init');
const path = require('path');

require('ts-node').register({
	dir: path.resolve(__dirname, '..'),
	project: path.resolve(__dirname, '..', 'tsconfig.json'),
	files: true,
	require: ['tsconfig-paths/register'],
});
require('./main');
