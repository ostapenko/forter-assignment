import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { format, isToday } from 'date-fns';

import { ContentType } from '../../shared/types';

@Component({
	selector: 'ui-message-bubble',
	templateUrl: 'message-bubble.component.html',
	styleUrls: ['message-bubble.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageBubbleComponent {
	@Input() content: Array<string> | string = '';
	@Input() contentType: ContentType = ContentType.plain;
	@Input() author = '';
	@Input() authorRole = '';
	@Input() timestamp: Date = new Date(0);
	@Input() showHeadline?: boolean | string;

	constructor() {}

	shouldShowHeadline(): boolean {
		return Boolean(this.showHeadline || this.showHeadline === '');
	}

	shouldShowHeadlineRole(): boolean {
		return this.authorRole !== '';
	}

	contentAsArray(): string[] {
		return Array.isArray(this.content) ? this.content : [this.content];
	}

	linesTrackBy(index: number, line: string): string {
		return line;
	}

	contentAsRichText(): string {
		return Array.isArray(this.content) ? this.content.join(' <br/>') : this.content;
	}

	multiline(): boolean {
		return this.contentType === ContentType.plain;
	}

	richText(): boolean {
		return this.contentType === ContentType.richText;
	}

	avatarUrl(): string {
		return `https://robohash.org/${this.author}?size=96x96&bgset=bg2`;
	}

	formatDate(): string {
		const today = isToday(this.timestamp);
		if (today) {
			return format(this.timestamp, 'H:mm');
		}
		return format(this.timestamp, 'MMM, d');
	}

	formatDateForLabel(): string {
		return format(this.timestamp, 'MMMM, d, HH:mm:ss');
	}
}
