# Forter Assignment
> Chuck Norris Chat by a.ostapenko

## Deployment

Application is ready to use on https://forter-assignment.vercel.app/. To interact with the bot, start with the command `/help` in the chat.

Storybook is deployed on https://forter-assignment.vercel.app/storybook-static/ (with the leading slash).

Backend API is deployed and accessible on https://freshauto.shopstory.live/api/v2

Since the focus in the test task was on implementation of the chat itself, I haven't created any complicated authentication in the app. To switch between chat users, use a query parameter `?user=` and refresh the page. For instance:
- https://forter-assignment.vercel.app/?user=alex
- https://forter-assignment.vercel.app/?user=george
- https://forter-assignment.vercel.app/?user=sarah

## Cheatsheet

- `nvm use && npm run safe-install` - to switch to the correct Node.js version (see [nvm](https://github.com/nvm-sh/nvm), or just use Node.js 18.13+) and install all dependencies
- `npm run lint:all:fix` - to lint the entire project and autofix all remaining issues
- `npm run storybook` - to open Storybook with the list of all Application Components (http://localhost:6006/)
- `npm start` - to start Angular in development mode (http://localhost:4200/)
- `npm run backend-start` - to start backend server locally. Please note, you need to create a `.env` file in the project root as per an example
- `npm test` - **run tests!**
  ```markdown
  Test Suites: 5 passed, 5 total
  Tests:       21 passed, 21 total
  Snapshots:   4 passed, 4 total
  ```

### Local development

To be able to start the project on localhost, first you need to create a `.env` file in the project root based on the template. Credentials from the publicly deployed MongoDB instance I have provided in the email, supporting the submission of the assignment.

To start the project you need to run these two commands simultaneously in two different Terminals:
- `npm start` - to start Angular in development mode
- `npm run backend-start` - to start backend server locally

After that your API server will be ready on `http://localhost:3001/api/ping`

Your UI application in development mode will be ready on `http://localhost:4200?API_BASE_URL=http:%2F%2Flocalhost:3001`

To control which API you use, you may change the query parameter `?API_BASE_URL=`:
- `http://localhost:4200?API_BASE_URL=http:%2F%2Flocalhost:3001` - for API running locally
- `http://localhost:4200?API_BASE_URL=https:%2F%2Ffreshauto.shopstory.live` - for a deployed API

### Project structure

This project is created using Angular CLI:
- clientside application is located in `src` folde
  - `src/app/ui-kit` to demonstrate my skills in creating UI components and maintaining a UI Kit I have created a small set of components that I use in the application
  - use `npm run storybook` - to open the Storybook with the list of playable components
  - `src/app/core/services` - main services that implement clientside Chat interactions
- serverside application is inside `backend` folder
  - `backend/modules/bot-engine.ts` module responsible for the Chat Bot implementation

### Known issues / topics for discussion on the final interviews

Unfortunately, I a little mismanaged the time given for this test assignment. I haven't had enough time to properly finish work on this test task and I couldn't implement all ideas I had been planning for this test project. 
- The virtual scroll library used in the implementation is known to have issues with the lists of items of different sizes. I had previously worked on projects requiring such use case and I know how this must be implemented. Back in the time I even created a library that would support flawless virtual scroll for such use cases, though I couldn't open source it due to some company rules and security policies.
- In the test task I focused mainly on the visual part and on UI Components. Consequently, the implementation of the Chat itself has room for improvement
  - Interactivity of the Chat isn't the greatest
  - For the chat synchronization I use a short-polling, though there are more stable and performant approaches, for example Server Site Events and Websockets. Those are the great topics to discuss on the final interviews, I will be able to share thoughts on how this should properly be implemented

### Final project submission remarks

Hopefully, I haven't missed any important details in this README. I tried to keep it simple and informative. Although, if you miss something or need help in running/playing with the application, feel free to reach me out for clarifying comments.

Look forward to the Code Review interviews!
