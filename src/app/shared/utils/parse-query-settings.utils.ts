interface QuerySettings {
	baseUrl?: string;
	user?: string;
}

export const API_BASE_URL = 'API_BASE_URL';
export const USER = 'user';
export const parseQuerySettings = (search = window.location.search): QuerySettings => {
	const urlParams = new URLSearchParams(search);
	return {
		baseUrl: urlParams.get(API_BASE_URL)?.trim(),
		user: urlParams.get(USER)?.trim().replaceAll(/\s/g, '').replaceAll(/\W/gi, ''),
	};
};
