import mongoose, { Connection } from 'mongoose';

export enum ContentType {
	plain = 'plain',
	richText = 'richText',
}

export interface IChatEntry {
	id: string;
	author: string;
	sent: Date;
	content: string;
	contentType: ContentType;
}

export interface IChatEntryModel extends Omit<IChatEntry, 'id'>, mongoose.Document {}

const ChatEntrySchema = new mongoose.Schema(
	{
		author: { type: mongoose.Schema.Types.String, required: true },
		sent: { type: mongoose.Schema.Types.Date, required: true },
		content: { type: mongoose.Schema.Types.String, required: true },
		contentType: {
			type: mongoose.Schema.Types.String,
			required: false,
			enum: [ContentType.plain, ContentType.richText],
		},
	},
	{ collection: 'entries', timestamps: true },
);

const defineChatEntries = (connection: Connection) =>
	connection.model<IChatEntryModel>('ChatEntries', ChatEntrySchema);

export { defineChatEntries };
