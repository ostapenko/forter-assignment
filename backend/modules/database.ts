import Debug from 'debug';
import { Connection } from 'mongoose';

import { ContentType, defineChatEntries, IChatEntry } from '../models/ChatEntries';
import { defineUsers, IUser, UserRole } from '../models/Users';

const debug = Debug('database-model');

export default class DatabaseModule {
	private readonly Users = defineUsers(this.connection);
	private readonly ChatEntries = defineChatEntries(this.connection);
	constructor(private readonly connection: Connection) {
		debug('constructor');
	}

	async touchUser(nickname: string, role: UserRole = UserRole.user): Promise<void> {
		const maybeUser = await this.Users.findOne({ nickname });

		const now = new Date();

		if (maybeUser) {
			maybeUser.lastSeen = now;
			await maybeUser.save();
			return;
		}

		await this.Users.create({
			nickname,
			role,
			lastSeen: now,
		});
	}

	async submitMessage(
		message: string,
		contentType: ContentType,
		author: { nickname: string; role: UserRole },
	): Promise<void> {
		const now = new Date();
		await Promise.all([
			this.touchUser(author.nickname, author.role),
			this.ChatEntries.create({
				author: author.nickname,
				sent: now,
				content: message,
				contentType: contentType,
			}),
		]);
	}

	async queryEntries(noLatterThan?: Date | null, limit = 100): Promise<IChatEntry[]> {
		const entries = await this.ChatEntries.find(
			noLatterThan
				? {
						sent: { $lte: noLatterThan },
				  }
				: {},
		)
			.sort({ sent: 'desc' })
			.limit(limit);

		return entries
			.map((entryModel): IChatEntry => {
				const { author, sent, content, contentType } = entryModel;
				return { author, sent, content, id: String(entryModel._id), contentType };
			})
			.reverse();
	}

	async queryUsers(nicknames: string[]): Promise<IUser[]> {
		const users = await this.Users.find({
			nickname: { $in: nicknames },
		});

		return users.map((userModel): IUser => {
			const { lastSeen, nickname, role } = userModel;
			return { lastSeen, nickname, role, id: String(userModel._id) };
		});
	}
}
