import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { Meta, moduleMetadata, Story } from '@storybook/angular';

import { TextComponent } from './text.component';

export default {
	title: 'Text',
	component: TextComponent,
	decorators: [
		moduleMetadata({
			declarations: [TextComponent],
			imports: [BrowserModule, CommonModule],
		}),
	],
} as Meta;

export const TextBase: Story<TextComponent> = () => ({
	props: {},
	template: `
	<div style="padding: 16px; max-width: 480px">

		<ui-text mode="header">Header</ui-text>
		<ui-text mode="subheader">Subheader</ui-text>
		<ui-text>Base text</ui-text>
		<br>
		<ui-text mode="small">Small text</ui-text>
		<br>
		<ui-text mode="label" labelFor="field">Label text with attribute "for"</ui-text>
		<input type="text" id="field" style="margin-left: 8px">
		<br>
		<hr>
		<ui-text color="secondary">Secondary text</ui-text>
		<br>
		<ui-text color="dimmed">Dimmed text</ui-text>
		<br>
		<ui-text color="error">Error text</ui-text>
		<br>
		<ui-text bold>Bold text</ui-text>
		<br>
		<ui-text color="secondary" mode="pseudo-link">Pseudo link</ui-text>
		<br>
		<hr>

		<div style="padding: 16px; max-width: 240px; background-color: #fff">
			<ui-text color="dark">Always dark text</ui-text>
		</div>
		<div style="padding: 16px; max-width: 240px; background-color: #333">
			<ui-text color="light">Always light text</ui-text>
		</div>

	</div>
	`,
});

TextBase.story = {
	name: 'all modes',
};

export const TextExample: Story<TextComponent> = () => ({
	props: {},
	template: `
	<div style="padding: 16px; max-width: 480px">
		<ui-text mode="header">End-to-end data operating platform</ui-text>
		<br>
		<ui-text mode="subheader">See your data in a new light</ui-text>
		<br>
		<ui-text>
			Powered by <ui-text bold>machine learning and advanced analytics</ui-text>, Behavox is
			your organization’s single entry point for all internal data, transforming your blind
			spots into competitive advantages by mitigating risk and identifying revenue
			opportunities.
		</ui-text>
		<ui-text color="secondary" block>© BEHAVOX 2020</ui-text>
	</div>
	`,
});

TextExample.story = {
	name: 'example content',
};
