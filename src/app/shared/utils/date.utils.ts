export const isDate = (value?: Date | null): value is Date => {
	return value instanceof Date;
};

export const isDatesSame = (valueA?: Date | null, valueB?: Date | null): boolean => {
	if (isDate(valueA) && isDate(valueB) && valueA.valueOf() === valueB.valueOf()) {
		return true;
	}

	return valueA === valueB;
};

export const parseDate = (value?: Date | string | null): Date | null => {
	if (!value) {
		return null;
	}
	const date = new Date(value);
	if (date.toString() === 'Invalid Date') {
		return null;
	}

	return date;
};

export const convertDateToUTC = (date: Date): Date => {
	return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0));
};

export const toDayStart = (date: Date): Date => {
	return new Date(
		Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 0, 0, 0, 0),
	);
};

export const toDayEnd = (date: Date): Date => {
	return new Date(
		Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 23, 59, 59, 999),
	);
};
