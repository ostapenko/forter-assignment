import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
	imports: [BrowserModule, HttpClientModule],
	providers: [],
	bootstrap: [],
	exports: [],
})
export class SharedModule {}
