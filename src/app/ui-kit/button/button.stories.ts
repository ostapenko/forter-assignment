import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import send from '@material-design-icons/svg/outlined/send.svg';
import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { IconComponent } from '../icon/icon.component';
import { TextComponent } from '../text/text.component';
import { ButtonComponent } from './button.component';

export default {
	title: 'Button',
	component: ButtonComponent,
	decorators: [
		moduleMetadata({
			declarations: [ButtonComponent, IconComponent, TextComponent],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				RouterModule.forRoot([{ path: '**', component: TextComponent }]),
				AngularSvgIconModule.forRoot(),
			],
			providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
		}),
	],
} as Meta;

export const ButtonBase: Story<ButtonComponent> = () => ({
	props: {
		send,
	},
	template: `
		<div style="padding: 24px; max-width: 480px;">
			<span style="margin-right: 8px"><ui-button color="primary">Submit</ui-button></span>
			<ui-button [icon]="send"></ui-button>
			<ui-button [icon]="send" color="transparent"></ui-button>
			<ui-button [icon]="send" color="transparent" disabled></ui-button>
			<ui-button [icon]="send" block>Block Button</ui-button>
		</div>
		<div style="padding: 24px; max-width: 480px; background-color: #333">
			<span style="margin-right: 8px"><ui-button [icon]="send" color="dark">Always Dark Button</ui-button></span>
			<span style="margin-right: 8px"><ui-button [icon]="send" color="light">Always Light Button</ui-button></span>
		</div>
	`,
});

ButtonBase.story = {
	name: 'examples',
};
