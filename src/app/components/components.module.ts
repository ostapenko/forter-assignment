import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AngularSvgIconModule } from 'angular-svg-icon';

import { SharedModule } from '../shared/shared.module';
import { UiKitModule } from '../ui-kit/ui-kit.module';
import { ChatroomEntriesComponent } from './chatroom-entries/chatroom-entries.component';
import { EntryRendererComponent } from './chatroom-entries/entry-renderer/entry-renderer.component';
import { ChatroomFooterComponent } from './chatroom-footer/chatroom-footer.component';
import { ChatroomHeaderComponent } from './chatroom-header/chatroom-header.component';

@NgModule({
	exports: [
		ChatroomHeaderComponent,
		ChatroomFooterComponent,
		ChatroomEntriesComponent,
		EntryRendererComponent,
	],
	declarations: [
		ChatroomHeaderComponent,
		ChatroomFooterComponent,
		ChatroomEntriesComponent,
		EntryRendererComponent,
	],
	imports: [
		RouterModule,
		CommonModule,
		AngularSvgIconModule,
		ReactiveFormsModule,
		FormsModule,
		UiKitModule,
		SharedModule,
	],
})
export class ComponentsModule {}
