import { ArrayDataSource, DataSource } from '@angular/cdk/collections';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import {
	AfterViewInit,
	Component,
	Input,
	OnDestroy,
	OnInit,
	QueryList,
	ViewChild,
	ViewChildren,
} from '@angular/core';
import { map, ReplaySubject, tap } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { EnrichedEntry } from '../../shared/types';
import { EntryRendererComponent } from './entry-renderer/entry-renderer.component';

@Component({
	selector: 'forter-chatroom-entries',
	templateUrl: 'chatroom-entries.component.html',
	styleUrls: ['chatroom-entries.component.css'],
})
export class ChatroomEntriesComponent implements OnInit, OnDestroy, AfterViewInit {
	@Input() ds: DataSource<EnrichedEntry> = new ArrayDataSource([]);
	@Input() dsArray: EnrichedEntry[] = [];
	@ViewChild(CdkVirtualScrollViewport) virtualScrollViewport?: CdkVirtualScrollViewport;
	@ViewChildren(EntryRendererComponent) entriesRef!: QueryList<EntryRendererComponent>;

	private destroy: ReplaySubject<null> = new ReplaySubject(1);

	constructor() {}

	ngOnInit(): void {
		this.virtualScrollViewport?.scrolledIndexChange.subscribe(index =>
			console.log('scrolledIndexChange', index),
		);
	}

	ngAfterViewInit() {
		this.entriesRef.changes
			.pipe(
				map((query: QueryList<EntryRendererComponent>) => query.last),
				distinctUntilChanged((previous, current) => previous.entry.id === current.entry.id),
				tap(element => console.log(element)),
				takeUntil(this.destroy),
			)
			.subscribe(lastChatEntryComponent => {
				lastChatEntryComponent.element.nativeElement.scrollIntoView({ behavior: 'smooth' });
			});
	}

	entriesTrackBy(i: number, entry: EnrichedEntry): string {
		return entry.id;
	}

	entries() {
		return this.dsArray;
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();
	}
}
