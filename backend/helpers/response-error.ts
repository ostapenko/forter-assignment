import { StatusCodes } from 'http-status-codes';

export enum ResponseErrorCode {
	internal = 'internal',
	bad_request = 'bad_request',
	invalid = 'invalid',
}

export default class ResponseError extends Error {
	constructor(public statusCode: number, public errorCode: ResponseErrorCode, message = '') {
		super(message);
	}
}

export class InternalError extends ResponseError {
	constructor() {
		super(StatusCodes.INTERNAL_SERVER_ERROR, ResponseErrorCode.internal, 'internal error');
	}
}
