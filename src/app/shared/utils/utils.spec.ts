import { expect } from '@jest/globals';

import { mockAvatarUrl } from './avatar.utils';
import { API_BASE_URL, parseQuerySettings, USER } from './parse-query-settings.utils';

describe('Util Modules', () => {
	it('mockAvatarUrl', () => {
		expect(mockAvatarUrl()).toBe(`https://robohash.org/anon?size=96x96&bgset=bg2`);
		expect(mockAvatarUrl('username')).toBe(
			`https://robohash.org/username?size=96x96&bgset=bg2`,
		);
		expect(mockAvatarUrl('username with spaces')).toBe(
			`https://robohash.org/username_with_spaces?size=96x96&bgset=bg2`,
		);
	});

	it('parseQuerySettings', () => {
		const baseUrlExample = 'https://api.example.com';
		const userExample = 'username';
		const testQuery = `?${API_BASE_URL}=${encodeURIComponent(
			baseUrlExample,
		)}&${USER}=${encodeURIComponent(userExample)}`;
		expect(parseQuerySettings(testQuery)).toEqual({
			baseUrl: baseUrlExample,
			user: userExample,
		});
	});
});
