import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { SafeRichTextPipe } from './pipes/safe-rich-text.pipe';
import { ChatroomService } from './services/chatroom.service';
import { ChatroomUtilsService } from './services/chatroom-utils.service';

@NgModule({
	imports: [HttpClientModule],
	providers: [ChatroomService, ChatroomUtilsService],
	exports: [SafeRichTextPipe],
	declarations: [SafeRichTextPipe],
})
export class CoreModule {}
