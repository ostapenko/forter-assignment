import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

type AvatarSize = 'large' | 'small' | 'tiny';
type BorderMode = 'highlight' | 'online' | 'none';

@Component({
	selector: 'ui-avatar',
	templateUrl: 'avatar.component.html',
	styleUrls: ['avatar.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AvatarComponent implements OnInit {
	@Input() src = '';
	@Input() alt = '';
	@Input() size: AvatarSize = 'large';
	@Input() border?: BorderMode;

	constructor() {}

	ngOnInit(): void {}
}
