/* eslint-disable @typescript-eslint/ban-ts-comment */
import 'jest-preset-angular/setup-jest';

// @ts-ignore
import { TextDecoder, TextEncoder } from 'util';

// @ts-ignore
global.TextEncoder = TextEncoder;
// @ts-ignore
global.TextDecoder = TextDecoder;
