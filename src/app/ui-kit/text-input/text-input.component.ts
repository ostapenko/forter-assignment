import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	forwardRef,
	Input,
	OnInit,
	ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
	selector: 'ui-text-input',
	templateUrl: 'text-input.component.html',
	styleUrls: ['text-input.component.css'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			// eslint-disable-next-line @typescript-eslint/no-use-before-define
			useExisting: forwardRef(() => TextInputComponent),
			multi: true,
		},
	],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextInputComponent implements OnInit, ControlValueAccessor {
	@Input() placeholder?: string = '';

	@ViewChild('input') inputElementRef?: ElementRef<HTMLInputElement>;
	private onChange?: (value: string) => void;
	private onTouched?: () => void;

	constructor(private changeDetectorRef: ChangeDetectorRef) {}

	_value = '';

	get value(): string {
		return this.inputElementRef?.nativeElement.value ?? '';
	}

	set value(value: string) {
		this._value = value ?? '';
		this.changeDetectorRef.detectChanges();
	}

	ngOnInit(): void {}

	onChangeInput(): void {
		this.writeValue(this.value);
	}

	registerOnChange(fn?: () => void): void {
		this.onChange = fn;
	}

	registerOnTouched(fn?: () => void): void {
		this.onTouched = fn;
	}

	writeValue(value: string): void {
		this.value = value ?? '';
		if (typeof this.onTouched === 'function') {
			this.onTouched();
		}
		if (typeof this.onChange === 'function') {
			this.onChange(value ?? '');
		}
	}
}
