import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { forwardRef } from '@angular/core';
import { FormControl, FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { Meta, moduleMetadata, Story } from '@storybook/angular';

import { TextComponent } from '../text/text.component';
import { ToggleComponent } from './toggle.component';

export default {
	title: 'Toggle',
	component: ToggleComponent,
	decorators: [
		moduleMetadata({
			declarations: [ToggleComponent, TextComponent],
			imports: [BrowserModule, CommonModule, FormsModule, ReactiveFormsModule],
			providers: [
				{ provide: APP_BASE_HREF, useValue: '/' },
				{
					provide: NG_VALUE_ACCESSOR,
					// eslint-disable-next-line @typescript-eslint/no-use-before-define
					useExisting: forwardRef(() => ToggleComponent),
					multi: true,
				},
			],
		}),
	],
} as Meta;

export const ToggleBase: Story<ToggleComponent> = () => ({
	props: {
		firstCheckbox: true,
		secondCheckbox: false,
		thirdCheckbox: new FormControl(true),
	},
	template: `
		<div style="padding: 24px; max-width: 480px;">
			<ui-toggle [(ngModel)]="firstCheckbox" style="margin-bottom: 4px">First toggle</ui-toggle> <br>
			<ui-toggle [(ngModel)]="secondCheckbox" style="margin-bottom: 4px">Second toggle</ui-toggle> <br>
			<ui-toggle [formControl]="thirdCheckbox">Third toggle</ui-toggle> <br> <br>
			<code>| first = {{firstCheckbox}} |</code>
			<code>| second = {{secondCheckbox}} |</code>
			<code>| third = {{thirdCheckbox.value}} |</code>
		</div>
	`,
});

ToggleBase.story = {
	name: 'example',
};
