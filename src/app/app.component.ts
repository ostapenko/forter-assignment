import { ArrayDataSource, DataSource } from '@angular/cdk/collections';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, ReplaySubject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { ChatroomService } from './core/services/chatroom.service';
import { EnhancedUser, EnrichedEntry } from './shared/types';

import send from '!!raw-loader!@material-design-icons/svg/outlined/send.svg';

@Component({
	selector: 'forter-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
	users: EnhancedUser[] = [];
	entriesDataSource: DataSource<EnrichedEntry> = new ArrayDataSource<EnrichedEntry>([]);
	entriesDataSourceArray: EnrichedEntry[] = [];
	send = send;
	private destroy: ReplaySubject<null> = new ReplaySubject(1);

	constructor(private readonly chatroomService: ChatroomService) {}

	ngOnInit(): void {
		this.chatroomService.chatroomObservable$
			.pipe(
				map(state => state.users),
				distinctUntilChanged(),
				takeUntil(this.destroy),
			)
			.subscribe(usersArray => {
				this.users = Array.from(usersArray.values());
			});

		this.chatroomService.chatroomObservable$
			.pipe(
				map(state => state.entries),
				distinctUntilChanged(),
				takeUntil(this.destroy),
			)
			.subscribe(entries => (this.entriesDataSourceArray = entries));

		this.entriesDataSource = new ArrayDataSource<EnrichedEntry>([]);
	}

	ngOnDestroy(): void {
		this.destroy.next(null);
		this.destroy.complete();
	}

	onNewMessageSubmitted($eventContent: string) {
		this.chatroomService
			.submitMessage($eventContent)
			.pipe(takeUntil(this.destroy))
			.subscribe(() => this.chatroomService.updateChatroom());
	}
}
