import { ArrayDataSource } from '@angular/cdk/collections';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import send from '@material-design-icons/svg/outlined/send.svg';
import type { Meta, Story } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { addSeconds } from 'date-fns';

import { ComponentsModule } from '../components/components.module';
import { SharedModule } from '../shared/shared.module';
import { ContentType, EnhancedUser, EnrichedEntry, UserRole } from '../shared/types';
import { mockAvatarUrl } from '../shared/utils/avatar.utils';
import { UiKitModule } from '../ui-kit/ui-kit.module';

export default {
	title: 'Chatroom',
	decorators: [
		moduleMetadata({
			declarations: [],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				AngularSvgIconModule.forRoot(),
				UiKitModule,
				ComponentsModule,
				SharedModule,
			],
		}),
	],
} as Meta;

const mockEntries: Array<EnrichedEntry> = [
	{
		author: 'Terminator',
		id: '',
		authorRole: UserRole.user,
		timestamp: new Date(),
		timestampPrev: addSeconds(new Date(), 20),
		content: ['Hi there'],
		contentType: ContentType.plain,

		isBot: false,
		isMe: false,
		firstEntryOfThatAuthor: true,
	},
	{
		author: 'Terminator',
		id: '',
		authorRole: UserRole.user,
		timestamp: addSeconds(new Date(), 12),
		timestampPrev: addSeconds(new Date(), 20),
		content: ['Attached to this email is a full description of the challenge.'],
		contentType: ContentType.plain,

		isBot: false,
		isMe: false,
		firstEntryOfThatAuthor: false,
	},
	{
		author: 'Terminator',
		id: '',
		authorRole: UserRole.user,
		timestamp: addSeconds(new Date(), 20),
		timestampPrev: addSeconds(new Date(), 20),
		content: [
			'You’ll have a link to a skeleton of the code you’ll need to get started. Be creative (design & UX) and playful (chatbot with an attitude?), yet also serious and thoughtful. Add relevant tests, write robust code that you’ll be proud of, etc.',
		],
		contentType: ContentType.plain,

		isBot: false,
		isMe: false,
		firstEntryOfThatAuthor: false,
	},
	{
		author: 'Terminator',
		id: '',
		authorRole: UserRole.user,
		timestamp: addSeconds(new Date(), 22),
		timestampPrev: addSeconds(new Date(), 20),
		content: ["If you have any questions please contact Yosef or Ori who are CC'd"],
		contentType: ContentType.plain,

		isBot: false,
		isMe: false,
		firstEntryOfThatAuthor: false,
	},
	{
		author: 'Chuck Norris Bot',
		id: '',
		authorRole: UserRole.bot,
		timestamp: addSeconds(new Date(), 22),
		timestampPrev: addSeconds(new Date(), 20),
		content: ["If you have any questions please contact Yosef or Ori who are CC'd."],
		contentType: ContentType.plain,

		isBot: true,
		isMe: false,
		firstEntryOfThatAuthor: true,
	},
	{
		author: 'SaraConnor',
		id: '',
		authorRole: UserRole.user,
		timestamp: new Date(),
		timestampPrev: addSeconds(new Date(), 20),
		content: ['Hi there'],
		contentType: ContentType.plain,

		isBot: false,
		isMe: false,
		firstEntryOfThatAuthor: true,
	},
	{
		author: 'SaraConnor',
		id: '',
		authorRole: UserRole.user,
		timestamp: addSeconds(new Date(), 12),
		timestampPrev: addSeconds(new Date(), 20),
		content: ['Attached to this email is a full description of the challenge.'],
		contentType: ContentType.plain,

		isBot: false,
		isMe: false,
		firstEntryOfThatAuthor: false,
	},
	{
		author: 'SaraConnor',
		id: '',
		authorRole: UserRole.user,
		timestamp: addSeconds(new Date(), 20),
		timestampPrev: addSeconds(new Date(), 20),
		content: [
			'You’ll have a link to a skeleton of the code you’ll need to get started. Be creative (design & UX) and playful (chatbot with an attitude?), yet also serious and thoughtful. Add relevant tests, write robust code that you’ll be proud of, etc.',
		],
		contentType: ContentType.plain,

		isBot: false,
		isMe: false,
		firstEntryOfThatAuthor: false,
	},
	{
		author: 'SaraConnor',
		id: '',
		authorRole: UserRole.user,
		timestamp: addSeconds(new Date(), 22),
		timestampPrev: addSeconds(new Date(), 20),
		content: ["If you have any questions please contact Yosef or Ori who are CC'd."],
		contentType: ContentType.plain,

		isBot: false,
		isMe: false,
		firstEntryOfThatAuthor: false,
	},
];

const dsArray = [
	...mockEntries,
	...mockEntries,
	...mockEntries,
	...mockEntries,
	...mockEntries,
	...mockEntries,
	...mockEntries,
];
export const ChatroomBase: Story = () => ({
	props: {
		send,
		users: [
			{
				nickname: 'user1',
				avatarUrl: mockAvatarUrl('user1'),
				role: UserRole.user,
				isActive: true,
				lastSeen: new Date(0),
			},
			{
				nickname: 'user2',
				avatarUrl: mockAvatarUrl('user2'),
				role: UserRole.user,
				isActive: true,
				lastSeen: new Date(1),
			},
			{
				nickname: 'user3',
				avatarUrl: mockAvatarUrl('user3'),
				role: UserRole.user,
				isActive: true,
				lastSeen: new Date(0),
			},
			{
				nickname: 'user4',
				avatarUrl: mockAvatarUrl('user4'),
				role: UserRole.user,
				isActive: true,
				lastSeen: new Date(0),
			},
			{
				nickname: 'user5',
				avatarUrl: mockAvatarUrl('user5'),
				role: UserRole.user,
				isActive: false,
				lastSeen: new Date(0),
			},
			{
				nickname: 'mustBeFirst',
				avatarUrl: mockAvatarUrl('mustBeFirst'),
				role: UserRole.user,
				isActive: true,
				lastSeen: new Date(),
			},
			{
				nickname: 'user6',
				avatarUrl: mockAvatarUrl('user6'),
				role: UserRole.user,
				isActive: true,
				lastSeen: new Date(10),
			},
			{
				nickname: 'bot',
				avatarUrl: mockAvatarUrl('bot'),
				role: UserRole.bot,
				isActive: true,
				lastSeen: new Date(),
			},
			{
				nickname: 'user7',
				avatarUrl: mockAvatarUrl('user7'),
				role: UserRole.user,
				isActive: true,
				lastSeen: new Date(2),
			},
		] as EnhancedUser[],

		onNewMessageSent: ($event: any) => {
			console.info($event);
		},

		dsArray,
		ds: new ArrayDataSource<EnrichedEntry>(dsArray),
	},
	template: `
		<div style="width: 2000px; height: 900px; position: fixed; left: -200px; top: -20px; background-color: white; background-image: url('../../assets/wallpapers.jpg'); background-size: cover;"></div>
			<div style="width: 320px; height: 690px; margin-left: 200px; margin-top: 40px;">
				<ui-panel>
				<div style="font-size: 0; display: flex; height: 100%; position: relative; overflow: hidden; flex-direction: column; gap: 0;">
					<forter-chatroom-header [users]="users"></forter-chatroom-header>
					<forter-chatroom-entries [ds]="ds" [dsArray]="dsArray" style="flex-grow: 1"></forter-chatroom-entries>
					<forter-chatroom-footer [sendIcon]="send" (newMessageSent)="onNewMessageSent($event)"></forter-chatroom-footer>
					</div>
				</ui-panel>
			</div>
	`,
});

ChatroomBase.story = {
	name: 'main state',
};
