interface ApplicationConfig {
	MONGODB_CONNECTION_STRING: string;
	LISTEN_PORT: string;
}

const conf: Partial<ApplicationConfig> = {
	MONGODB_CONNECTION_STRING: process.env['MONGODB_CONNECTION_STRING'],
	LISTEN_PORT: process.env['LISTEN_PORT'] || '3000',
};

const isValidConfig = (conf: unknown): conf is ApplicationConfig => {
	return Boolean(
		conf &&
			typeof conf === 'object' &&
			'MONGODB_CONNECTION_STRING' in conf &&
			'LISTEN_PORT' in conf,
	);
};

function fail(): never {
	throw new Error(`invalid config`);
}

const config: ApplicationConfig = isValidConfig(conf) ? conf : fail();

export default config;
