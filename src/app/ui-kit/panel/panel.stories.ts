import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import type { Meta, Story } from '@storybook/angular';
import { moduleMetadata } from '@storybook/angular';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { AvatarComponent } from '../avatar/avatar.component';
import { IconComponent } from '../icon/icon.component';
import { SpinnerComponent } from '../spinner/spinner.component';
import { TextComponent } from '../text/text.component';
import { PanelComponent } from './panel.component';

export default {
	title: 'Panel',
	component: PanelComponent,
	decorators: [
		moduleMetadata({
			declarations: [
				PanelComponent,
				IconComponent,
				TextComponent,
				AvatarComponent,
				SpinnerComponent,
			],
			imports: [
				BrowserModule,
				CommonModule,
				HttpClientModule,
				AngularSvgIconModule.forRoot(),
			],
		}),
	],
} as Meta;

export const PanelBase: Story<PanelComponent> = () => ({
	props: {},
	template: `
		<div style="width: 678px; height: 500px; border: black 1px solid; background-color: white; background-image: url('../../../assets/wallpapers.jpg'); background-size: cover;">
		<div style="width: 500px; height: 300px; position: relative; top: 50%; left: 50%; transform: translate(-50%, -50%);">
			<ui-panel>
			<div style="padding: 6px 12px;">
				<h1>Inner Content</h1>
				<ui-spinner></ui-spinner>
				<hr>
				<ui-text>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At distinctio eveniet odit. Accusantium cumque earum enim exercitationem labore necessitatibus reprehenderit, sunt. Dolore, eius explicabo facilis harum illo labore molestiae nisi non numquam perspiciatis! Adipisci corporis dolorem eum illum mollitia quasi recusandae saepe voluptatibus. A accusamus blanditiis cupiditate deleniti distinctio eaque fugit incidunt iusto magnam neque numquam perspiciatis quam quasi rem repellat saepe sequi ullam ut vel veniam, voluptatem voluptatibus voluptatum? Amet consequuntur cumque doloremque magni pariatur qui sapiente sit? Animi cum deserunt dolore ipsum itaque magni molestias nobis obcaecati qui quod. Asperiores blanditiis dignissimos dolor ducimus esse exercitationem ipsam itaque iusto laborum maiores nobis nulla odio odit omnis optio praesentium rem sapiente similique sint, ut vel veritatis voluptatem. Assumenda atque, blanditiis debitis dolorem eius est et fugit hic id illum ipsa itaque laborum magnam minus neque non omnis placeat possimus quas quasi quod repellendus repudiandae saepe sapiente sed? Modi, odio.</ui-text>
				</div>
			</ui-panel>

			</div>
		</div>
	`,
});

PanelBase.story = {
	name: 'examples',
};
