export const mockAvatarUrl = (nickname?: string): string => {
	return `https://robohash.org/${encodeURIComponent(
		nickname?.replaceAll(/\W/g, '_') || 'anon',
	)}?size=96x96&bgset=bg2`;
};
